// 处理 textarea 的高度
// 需要 jQuery
// http://segmentfault.com/q/1010000000095238#a-1020000000097663

$('textarea').keyup(function (event) {
    var textarea = $(this);
    var div = $("#for-textarea");

    var html = textarea.val()
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/'/g, '&#039;')
        .replace(/"/g, '&quot;')
        .replace(/ /g, '&nbsp;')
        .replace(/((&nbsp;)*)&nbsp;/g, '$1 ')
        .replace(/\n/g, '<br />')
        .replace(/<br \/>[ ]*$/, '<br />-')
        .replace(/<br \/> /g, '<br />&nbsp;');

    div.html(html);
    em = parseInt($(this).css("font-size").slice(0, -2));
    height = parseInt(div.height() + 1.5 * em);
    // 额外增加1.5em，防止出现滚动条
    textarea.height(height);

    if (event.which === 10 || event.which === 13) {
        $("body").scrollTop($("body").scrollTop() + 1.5 * em);
    }
    // 若弹起的是回车，则向下滚动1.5em
});

$('textarea').keypress(function (event) {
    if (event.ctrlKey && (event.which === 10 || event.which === 13)) {
        $("#" + $(this).attr("parent-form")).submit();
    }
    // Ctrl+Enter自动提交表单
});